export interface IUrl {
    slug: string,
    title: string,
    content: string,
    author?: string
}

export interface IUser {
    username: string | null
    isAdmin: boolean
}

export interface IAuthRoute {
    username: string | null,
    children: JSX.Element
}