import { useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { useAuth } from "../context/auth";

const Login = (): JSX.Element => {

    const navigate = useNavigate()

    const auth = useAuth();

    const [inputValue, setInputValue] = useState<string>('');

    const login = () => {
        auth.login(inputValue)
        navigate('/profile')
    }

    return (
        <>
            {auth.user && <Navigate to={'/profile'} />}
            <h1>Login</h1>
            <form >
                <label>
                    username
                    {/*  */}
                    <input
                        type="text"
                        value={inputValue}
                        onChange={(e) => setInputValue(e.target.value)}
                    />
                </label>
                <button type="button" onClick={inputValue !== '' ? () => login() : undefined}>Entrar</button>
            </form>
        </>
    );
};

export default Login;
