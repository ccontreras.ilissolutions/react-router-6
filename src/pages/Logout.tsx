import { FormEvent } from "react";
import { useAuth } from "../context/auth";
import { Navigate } from "react-router-dom";



const Logout = (): JSX.Element => {

    const auth = useAuth();

    const logout = (e: FormEvent) => {
        e.preventDefault();
        auth.logout();

    }

    return (
        <>
            {!auth.user && <Navigate to={'/login'} />}
            <h1>Logout</h1>
            <form onSubmit={logout}>
                <label>{auth.user?.username} seguro de salir</label>
                <button type="submit">Salir</button>
            </form>
        </>
    );
};

export default Logout;
