import { Navigate } from "react-router-dom";
import { useAuth } from "../context/auth";

const Profile = (): JSX.Element => {

    const auth = useAuth();

    return (
        <>
            {!auth.user && <Navigate to={'/login'} />}
            <h1>{auth.user?.username}</h1>
        </>

    );
};

export default Profile;
