import { PropsWithChildren } from "react";

interface PropsHome {
    example?: string
}

const Home = ({ example }: PropsWithChildren<PropsHome>): JSX.Element => {
    return (
        <>
            <h1>{example}</h1>
        </>
    );
};

export default Home;
