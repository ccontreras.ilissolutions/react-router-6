import { PropsWithChildren } from "react";
import BlogLink from "../components/BlogLink";
import { Outlet } from "react-router-dom";
import useBlogManagement from "../hook/useBlogManagement";

interface PropsBlog {
    example?: string
}

const Blog = ({ example }: PropsWithChildren<PropsBlog>): JSX.Element => {

    const {
        blogData,
    } = useBlogManagement();

    return (
        <>
            <h1>{example}</h1>
            <Outlet />
            <ul>
                {blogData.map(post => (
                    <BlogLink post={post} key={post.slug} />
                ))}
            </ul>
        </>
    );
};

export default Blog;
