import { PropsWithChildren, createContext, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { IUser } from "../interface/interfaces";

interface IAuth {
    user: IUser | null
    login: (username: string) => void
    logout: () => void
}

const adminList: string[] = ['Irisval', 'Carlos'];

const AuthContext = createContext({});

const AuthProvider = ({ children }: PropsWithChildren) => {

    const navigate = useNavigate();
    const [user, setUser] = useState<IUser | null>(null)


    const login = (username: string) => {
        const isAdmin = adminList.includes(username)
        setUser({ username, isAdmin })
        navigate('/profile')
    }

    const logout = () => {
        setUser(null)
        navigate('/')
    }



    const auth: IAuth = { user, login, logout }
    return (
        <AuthContext.Provider value={auth}>
            {children}
        </AuthContext.Provider>
    )
}

const useAuth = () => {
    const authContext = useContext(AuthContext) as IAuth;
    return authContext;
}

export { AuthProvider, useAuth }