import './App.css'
import { HashRouter, Route, Routes } from 'react-router-dom'
import Home from './pages/Home'
import Blog from './pages/Blog'
import Profile from './pages/Profile'
import Menu from './components/Menu'
import BlogPost from './components/BlogPost'
import Login from './pages/Login'
import Logout from './pages/Logout'
import { AuthProvider } from './context/auth'

function App() {


  return (
    <HashRouter>
      <AuthProvider>
        <Menu />
        <Routes>
          <Route path='/' element={<Home example='Home' />} />
          <Route path='/blog' element={<Blog example='Blog' />}>
            <Route path=':slug' element={<BlogPost />} />
          </Route>
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/profile' element={<Profile />} />
          <Route path='*' element={<p>Not Found</p>} />
        </Routes>
      </AuthProvider>
    </HashRouter>
  )
}

export default App
