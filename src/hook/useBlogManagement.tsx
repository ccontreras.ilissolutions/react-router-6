import { useState } from "react";
import { IUrl } from "../interface/interfaces";
import blogDataMock from "../mocks/blogData";

interface IUseBlogManagement {
    blogData: IUrl[]
    setBlogData: React.Dispatch<React.SetStateAction<IUrl[]>>
}

const useBlogManagement = (): IUseBlogManagement => {

    const [blogData, setBlogData] = useState<IUrl[]>(blogDataMock);

    return {
        blogData,
        setBlogData
    };

}

export default useBlogManagement;