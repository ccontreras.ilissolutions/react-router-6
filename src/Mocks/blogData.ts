import { IUrl } from "../interface/interfaces"

const blogDataMock: IUrl[] = []
blogDataMock.push(
    { slug: 'que-es-react', title: '¿Que es React?', content: 'un Frame de Javascript', author: 'Juan' },
    { slug: 'que-es-vue', title: '¿Que es Vue?', content: 'un Frame de Javascript', author: 'Maria' },
    { slug: 'que-es-angular', title: '¿Que es Angular?', content: 'un Frame de Javascript', author: 'Pedro' },
)

export default blogDataMock;