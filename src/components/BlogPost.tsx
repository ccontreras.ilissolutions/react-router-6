import { useNavigate, useParams } from "react-router-dom";
import { useAuth } from "../context/auth";
import { useState } from "react";
import useBlogManagement from "../hook/useBlogManagement";



const BlogPost = (): JSX.Element => {


    const auth = useAuth();

    const {
        blogData,
        setBlogData
    } = useBlogManagement();

    const { slug } = useParams();
    const navigate = useNavigate();
    const blogPost = blogData.find(post => post.slug === slug)

    const canDelete: boolean = auth.user?.isAdmin ?? auth.user?.username === blogPost?.author


    const deleteBlogspot = (): void => {
        const newData = blogData.filter(post => post.slug !== blogPost?.slug)
        setBlogData(newData)
        navigate('/blog')
    }

    const canEdit: boolean = auth.user?.isAdmin ?? auth.user?.username === blogPost?.author

    const [editMode, setEditMode] = useState<boolean>(false);

    const [slugInfo, setSlugInfo] = useState<string>(blogPost?.slug ?? '');
    const [title, setTitle] = useState<string>(blogPost?.title ?? '');
    const [content, setContent] = useState<string>(blogPost?.content ?? '');

    const onChangeSlug = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setSlugInfo(event.target.value)
    }

    const onChangeTitle = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setTitle(event.target.value)
    }

    const onChangeContent = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setContent(event.target.value)
    }

    const editBlogspot = (): void => {
        blogPost!.slug = slugInfo
        blogPost!.title = title
        blogPost!.content = content
        navigate(`/blog/${slugInfo}`)
        setEditMode(false)
    }

    return (
        <>
            {editMode ?
                <>
                    <input value={slugInfo} onChange={onChangeSlug}></input>
                    <input value={title} onChange={onChangeTitle}></input>
                    <input value={content} onChange={onChangeContent}></input>
                    <button onClick={() => setEditMode(false)}>Volver</button>
                    <button onClick={editBlogspot}>Guardar</button>
                    <p>{blogPost?.author}</p>
                </>
                :
                <>
                    <h2>{blogPost?.title}</h2>
                    <button onClick={() => navigate(-1)}>Volver</button>
                    <p>{blogPost?.content}</p>
                    <p>{blogPost?.author}</p>
                    {canEdit &&
                        <button onClick={() => setEditMode(true)}>Editar blogspot</button>
                    }
                </>
            }
            {
                canDelete &&
                <button onClick={deleteBlogspot}>Eliminar blogspot</button>
            }
        </>
    );
};

export default BlogPost;