import { PropsWithChildren } from "react";

interface PropsBase {
  example?: string
}

const Base = ({ example }: PropsWithChildren<PropsBase>): JSX.Element => {
  return (
    <>
      {example}
    </>
  );
};

export default Base;
