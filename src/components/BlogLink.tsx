import { PropsWithChildren } from "react";
import { Link } from "react-router-dom";
import { IUrl } from "../interface/interfaces";


interface PropsBlogLink {
    post: IUrl
}

const BlogLink = ({ post }: PropsWithChildren<PropsBlogLink>): JSX.Element => {
    return (
        <li>
            <Link to={`/blog/${post.slug}`}>{post.title}</Link>
        </li>
    );
};

export default BlogLink;
