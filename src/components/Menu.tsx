import { NavLink } from "react-router-dom";
import { useAuth } from "../context/auth";


const Menu = (): JSX.Element => {


    const routes = [
        { to: '/', text: 'Home', private: false },
        { to: '/blog', text: 'Blog', private: false },
        { to: '/profile', text: 'Profile', private: true },
        { to: '/login', text: 'Login', private: false, publicOnly: true },
        { to: '/logout', text: 'Logout', private: true }
    ]

    const auth = useAuth();

    return (
        <nav>
            <ul>
                {routes.map((route) => {
                    if (route.private && !auth.user) return null
                    if (route.publicOnly && auth.user) return null
                    return (
                        <li key={route.text}>
                            <NavLink
                                to={route.to}
                                className={({ isActive }) => isActive ? 'link-magenta' : 'link-blue'}
                            >{route.text}</NavLink>
                        </li>
                    )
                }
                )}
            </ul>
        </nav>
    );
};

export default Menu;
